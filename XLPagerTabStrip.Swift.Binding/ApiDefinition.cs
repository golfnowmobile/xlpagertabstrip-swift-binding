﻿using System;
using CoreGraphics;
using Foundation;
using ObjCRuntime;
using UIKit;


// @interface PagerTabStripViewController : UIViewController <UIScrollViewDelegate>
[BaseType(typeof(UIViewController), Name = "_TtC15XLPagerTabStrip27PagerTabStripViewController")]
interface PagerTabStripViewController : IUIScrollViewDelegate
{
    // @property (nonatomic, weak) UIScrollView * _Null_unspecified containerView __attribute__((iboutlet));
    [Export("containerView", ArgumentSemantic.Weak)]
    UIScrollView ContainerView { get; set; }

    // @property (readonly, copy, nonatomic) NSArray<UIViewController *> * _Nonnull viewControllers;
    [Export("viewControllers", ArgumentSemantic.Copy)]
    UIViewController[] ViewControllers { get; }

    // @property (readonly, nonatomic) NSInteger currentIndex;
    [Export("currentIndex")]
    nint CurrentIndex { get; }

    // @property (readonly, nonatomic) NSInteger preCurrentIndex;
    [Export("preCurrentIndex")]
    nint PreCurrentIndex { get; }

    // @property (readonly, nonatomic) CGFloat pageWidth;
    [Export("pageWidth")]
    nfloat PageWidth { get; }

    // @property (readonly, nonatomic) CGFloat scrollPercentage;
    [Export("scrollPercentage")]
    nfloat ScrollPercentage { get; }

    // -(void)viewDidLoad;
    [Export("viewDidLoad")]
    void ViewDidLoad();

    // -(void)viewWillAppear:(BOOL)animated;
    [Export("viewWillAppear:")]
    void ViewWillAppear(bool animated);

    // -(void)viewDidAppear:(BOOL)animated;
    [Export("viewDidAppear:")]
    void ViewDidAppear(bool animated);

    // -(void)viewWillDisappear:(BOOL)animated;
    [Export("viewWillDisappear:")]
    void ViewWillDisappear(bool animated);

    // -(void)viewDidDisappear:(BOOL)animated;
    [Export("viewDidDisappear:")]
    void ViewDidDisappear(bool animated);

    // -(void)viewDidLayoutSubviews;
    [Export("viewDidLayoutSubviews")]
    void ViewDidLayoutSubviews();

    // @property (readonly, nonatomic) BOOL shouldAutomaticallyForwardAppearanceMethods;
    [Export("shouldAutomaticallyForwardAppearanceMethods")]
    bool ShouldAutomaticallyForwardAppearanceMethods { get; }

    // -(void)moveToViewControllerAt:(NSInteger)index animated:(BOOL)animated;
    [Export("moveToViewControllerAt:animated:")]
    void MoveToViewControllerAt(nint index, bool animated);

    // -(void)moveToViewController:(UIViewController * _Nonnull)viewController animated:(BOOL)animated;
    [Export("moveToViewController:animated:")]
    void MoveToViewController(UIViewController viewController, bool animated);

    // -(NSArray<UIViewController *> * _Nonnull)viewControllersFor:(PagerTabStripViewController * _Nonnull)pagerTabStripController __attribute__((warn_unused_result));
    [Export("viewControllersFor:")]
    UIViewController[] ViewControllersFor(PagerTabStripViewController pagerTabStripController);

    // -(void)updateIfNeeded;
    [Export("updateIfNeeded")]
    void UpdateIfNeeded();

    // -(BOOL)canMoveToIndex:(NSInteger)index __attribute__((warn_unused_result));
    [Export("canMoveToIndex:")]
    bool CanMoveToIndex(nint index);

    // -(CGFloat)pageOffsetForChildAt:(NSInteger)index __attribute__((warn_unused_result));
    [Export("pageOffsetForChildAt:")]
    nfloat PageOffsetForChildAt(nint index);

    // -(CGFloat)offsetForChildAt:(NSInteger)index __attribute__((warn_unused_result));
    [Export("offsetForChildAt:")]
    nfloat OffsetForChildAt(nint index);

    // -(NSInteger)pageForContentOffset:(CGFloat)contentOffset __attribute__((warn_unused_result));
    [Export("pageForContentOffset:")]
    nint PageForContentOffset(nfloat contentOffset);

    // -(NSInteger)virtualPageForContentOffset:(CGFloat)contentOffset __attribute__((warn_unused_result));
    [Export("virtualPageForContentOffset:")]
    nint VirtualPageForContentOffset(nfloat contentOffset);

    // -(NSInteger)pageForVirtualPage:(NSInteger)virtualPage __attribute__((warn_unused_result));
    [Export("pageForVirtualPage:")]
    nint PageForVirtualPage(nint virtualPage);

    // -(void)updateContent;
    [Export("updateContent")]
    void UpdateContent();

    // -(void)reloadPagerTabStripView;
    [Export("reloadPagerTabStripView")]
    void ReloadPagerTabStripView();

    // -(void)scrollViewDidScroll:(UIScrollView * _Nonnull)scrollView;
    [Export("scrollViewDidScroll:")]
    void ScrollViewDidScroll(UIScrollView scrollView);

    // -(void)scrollViewWillBeginDragging:(UIScrollView * _Nonnull)scrollView;
    [Export("scrollViewWillBeginDragging:")]
    void ScrollViewWillBeginDragging(UIScrollView scrollView);

    // -(void)scrollViewDidEndScrollingAnimation:(UIScrollView * _Nonnull)scrollView;
    [Export("scrollViewDidEndScrollingAnimation:")]
    void ScrollViewDidEndScrollingAnimation(UIScrollView scrollView);

    // -(void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator> _Nonnull)coordinator;
    [Export("viewWillTransitionToSize:withTransitionCoordinator:")]
    void ViewWillTransitionToSize(CGSize size, IUIViewControllerTransitionCoordinator coordinator);

    // -(instancetype _Nonnull)initWithNibName:(NSString * _Nullable)nibNameOrNil bundle:(NSBundle * _Nullable)nibBundleOrNil __attribute__((objc_designated_initializer));
    [Export("initWithNibName:bundle:")]
    [DesignatedInitializer]
    IntPtr Constructor([NullAllowed] string nibNameOrNil, [NullAllowed] NSBundle nibBundleOrNil);

    // -(instancetype _Nullable)initWithCoder:(NSCoder * _Nonnull)aDecoder __attribute__((objc_designated_initializer));
    //[Export("initWithCoder:")]
    //[DesignatedInitializer]
    //IntPtr Constructor(NSCoder aDecoder);
}

// @interface BarPagerTabStripViewController : PagerTabStripViewController
[BaseType(typeof(PagerTabStripViewController), Name = "_TtC15XLPagerTabStrip30BarPagerTabStripViewController")]
interface BarPagerTabStripViewController
{
    // @property (nonatomic, weak) BarView * _Null_unspecified barView __attribute__((iboutlet));
    [Export("barView", ArgumentSemantic.Weak)]
    BarView BarView { get; set; }

    // -(instancetype _Nullable)initWithCoder:(NSCoder * _Nonnull)aDecoder __attribute__((objc_designated_initializer));
    //[Export("initWithCoder:")]
    //[DesignatedInitializer]
    //IntPtr Constructor(NSCoder aDecoder);

    // -(instancetype _Nonnull)initWithNibName:(NSString * _Nullable)nibNameOrNil bundle:(NSBundle * _Nullable)nibBundleOrNil __attribute__((objc_designated_initializer));
    [Export("initWithNibName:bundle:")]
    [DesignatedInitializer]
    IntPtr Constructor([NullAllowed] string nibNameOrNil, [NullAllowed] NSBundle nibBundleOrNil);

    // -(void)viewDidLoad;
    [Export("viewDidLoad")]
    void ViewDidLoad();

    // -(void)viewWillAppear:(BOOL)animated;
    [Export("viewWillAppear:")]
    void ViewWillAppear(bool animated);

    // -(void)reloadPagerTabStripView;
    [Export("reloadPagerTabStripView")]
    void ReloadPagerTabStripView();

    // -(void)updateIndicatorFor:(PagerTabStripViewController * _Nonnull)viewController fromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex withProgressPercentage:(CGFloat)progressPercentage indexWasChanged:(BOOL)indexWasChanged;
    [Export("updateIndicatorFor:fromIndex:toIndex:withProgressPercentage:indexWasChanged:")]
    void UpdateIndicatorFor(PagerTabStripViewController viewController, nint fromIndex, nint toIndex, nfloat progressPercentage, bool indexWasChanged);

    // -(void)updateIndicatorFor:(PagerTabStripViewController * _Nonnull)viewController fromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex;
    [Export("updateIndicatorFor:fromIndex:toIndex:")]
    void UpdateIndicatorFor(PagerTabStripViewController viewController, nint fromIndex, nint toIndex);
}

// @interface BarView : UIView
[BaseType(typeof(UIView), Name = "_TtC15XLPagerTabStrip7BarView")]
interface BarView
{
    // @property (nonatomic, strong) UIView * _Nonnull selectedBar;
    [Export("selectedBar", ArgumentSemantic.Strong)]
    UIView SelectedBar { get; set; }

    // -(instancetype _Nullable)initWithCoder:(NSCoder * _Nonnull)aDecoder __attribute__((objc_designated_initializer));
    //[Export("initWithCoder:")]
    //[DesignatedInitializer]
    //IntPtr Constructor(NSCoder aDecoder);

    // -(void)moveToIndex:(NSInteger)index animated:(BOOL)animated;
    [Export("moveToIndex:animated:")]
    void MoveToIndex(nint index, bool animated);

    // -(void)moveFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex progressPercentage:(CGFloat)progressPercentage;
    [Export("moveFromIndex:toIndex:progressPercentage:")]
    void MoveFromIndex(nint fromIndex, nint toIndex, nfloat progressPercentage);

    // -(void)layoutSubviews;
    [Export("layoutSubviews")]
    void LayoutSubviews();
}

// @interface ButtonBarPagerTabStripViewController : PagerTabStripViewController <UICollectionViewDelegate, UICollectionViewDataSource>
[BaseType(typeof(PagerTabStripViewController), Name = "_TtC15XLPagerTabStrip36ButtonBarPagerTabStripViewController")]
interface ButtonBarPagerTabStripViewController : IUICollectionViewDelegate, IUICollectionViewDataSource
{
    // @property (copy, nonatomic) void (^ _Nullable)(ButtonBarViewCell * _Nullable, ButtonBarViewCell * _Nullable, BOOL) changeCurrentIndex;
    [NullAllowed, Export("changeCurrentIndex", ArgumentSemantic.Copy)]
    Action<ButtonBarViewCell, ButtonBarViewCell, bool> ChangeCurrentIndex { get; set; }

    // @property (copy, nonatomic) void (^ _Nullable)(ButtonBarViewCell * _Nullable, ButtonBarViewCell * _Nullable, CGFloat, BOOL, BOOL) changeCurrentIndexProgressive;
    [NullAllowed, Export("changeCurrentIndexProgressive", ArgumentSemantic.Copy)]
    Action<ButtonBarViewCell, ButtonBarViewCell, nfloat, bool, bool> ChangeCurrentIndexProgressive { get; set; }

    // @property (nonatomic, weak) ButtonBarView * _Null_unspecified buttonBarView __attribute__((iboutlet));
    [Export("buttonBarView", ArgumentSemantic.Weak)]
    ButtonBarView ButtonBarView { get; set; }

    // -(instancetype _Nonnull)initWithNibName:(NSString * _Nullable)nibNameOrNil bundle:(NSBundle * _Nullable)nibBundleOrNil __attribute__((objc_designated_initializer));
    [Export("initWithNibName:bundle:")]
    [DesignatedInitializer]
    IntPtr Constructor([NullAllowed] string nibNameOrNil, [NullAllowed] NSBundle nibBundleOrNil);

    // -(instancetype _Nullable)initWithCoder:(NSCoder * _Nonnull)aDecoder __attribute__((objc_designated_initializer));
    //[Export("initWithCoder:")]
    //[DesignatedInitializer]
    //IntPtr Constructor(NSCoder aDecoder);

    // -(void)viewDidLoad;
    [Export("viewDidLoad")]
    void ViewDidLoad();

    // -(void)viewWillAppear:(BOOL)animated;
    [Export("viewWillAppear:")]
    void ViewWillAppear(bool animated);

    // -(void)viewDidLayoutSubviews;
    [Export("viewDidLayoutSubviews")]
    void ViewDidLayoutSubviews();

    // -(void)reloadPagerTabStripView;
    [Export("reloadPagerTabStripView")]
    void ReloadPagerTabStripView();

    // -(CGFloat)calculateStretchedCellWidths:(NSArray<NSNumber *> * _Nonnull)minimumCellWidths suggestedStretchedCellWidth:(CGFloat)suggestedStretchedCellWidth previousNumberOfLargeCells:(NSInteger)previousNumberOfLargeCells __attribute__((warn_unused_result));
    [Export("calculateStretchedCellWidths:suggestedStretchedCellWidth:previousNumberOfLargeCells:")]
    nfloat CalculateStretchedCellWidths(NSNumber[] minimumCellWidths, nfloat suggestedStretchedCellWidth, nint previousNumberOfLargeCells);

    // -(void)updateIndicatorFor:(PagerTabStripViewController * _Nonnull)viewController fromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex;
    [Export("updateIndicatorFor:fromIndex:toIndex:")]
    void UpdateIndicatorFor(PagerTabStripViewController viewController, nint fromIndex, nint toIndex);

    // -(void)updateIndicatorFor:(PagerTabStripViewController * _Nonnull)viewController fromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex withProgressPercentage:(CGFloat)progressPercentage indexWasChanged:(BOOL)indexWasChanged;
    [Export("updateIndicatorFor:fromIndex:toIndex:withProgressPercentage:indexWasChanged:")]
    void UpdateIndicatorFor(PagerTabStripViewController viewController, nint fromIndex, nint toIndex, nfloat progressPercentage, bool indexWasChanged);

    // -(CGSize)collectionView:(UICollectionView * _Nonnull)collectionView layout:(UICollectionViewLayout * _Nonnull)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath * _Nonnull)indexPath __attribute__((warn_unused_result));
    [Export("collectionView:layout:sizeForItemAtIndexPath:")]
    CGSize CollectionView(UICollectionView collectionView, UICollectionViewLayout collectionViewLayout, NSIndexPath indexPath);

    // -(void)collectionView:(UICollectionView * _Nonnull)collectionView didSelectItemAtIndexPath:(NSIndexPath * _Nonnull)indexPath;
    [Export("collectionView:didSelectItemAtIndexPath:")]
    void CollectionView(UICollectionView collectionView, NSIndexPath indexPath);

    // -(NSInteger)collectionView:(UICollectionView * _Nonnull)collectionView numberOfItemsInSection:(NSInteger)section __attribute__((warn_unused_result));
    [Export("collectionView:numberOfItemsInSection:")]
    nint GetItemsCount(UICollectionView collectionView, nint section);

    // -(UICollectionViewCell * _Nonnull)collectionView:(UICollectionView * _Nonnull)collectionView cellForItemAtIndexPath:(NSIndexPath * _Nonnull)indexPath __attribute__((warn_unused_result));
    [Export("collectionView:cellForItemAtIndexPath:")]
    UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath);

    // -(void)scrollViewDidEndScrollingAnimation:(UIScrollView * _Nonnull)scrollView;
    [Export("scrollViewDidEndScrollingAnimation:")]
    void ScrollViewDidEndScrollingAnimation(UIScrollView scrollView);
}

// @interface ButtonBarView : UICollectionView
[BaseType(typeof(UICollectionView), Name = "_TtC15XLPagerTabStrip13ButtonBarView")]
interface ButtonBarView
{
    // @property (nonatomic, strong) UIView * _Nonnull selectedBar;
    [Export("selectedBar", ArgumentSemantic.Strong)]
    UIView SelectedBar { get; set; }

    // -(instancetype _Nullable)initWithCoder:(NSCoder * _Nonnull)aDecoder __attribute__((objc_designated_initializer));
    //[Export("initWithCoder:")]
    //[DesignatedInitializer]
    //IntPtr Constructor(NSCoder aDecoder);

    // -(instancetype _Nonnull)initWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout * _Nonnull)layout __attribute__((objc_designated_initializer));
    [Export("initWithFrame:collectionViewLayout:")]
    [DesignatedInitializer]
    IntPtr Constructor(CGRect frame, UICollectionViewLayout layout);

    // -(void)layoutSubviews;
    [Export("layoutSubviews")]
    void LayoutSubviews();
}

// @interface ButtonBarViewCell : UICollectionViewCell
[BaseType(typeof(UICollectionViewCell), Name = "_TtC15XLPagerTabStrip17ButtonBarViewCell")]
interface ButtonBarViewCell
{
    // @property (nonatomic, strong) UIImageView * _Null_unspecified imageView __attribute__((iboutlet));
    [Export("imageView", ArgumentSemantic.Strong)]
    UIImageView ImageView { get; set; }

    // @property (nonatomic, strong) UILabel * _Null_unspecified label __attribute__((iboutlet));
    [Export("label", ArgumentSemantic.Strong)]
    UILabel Label { get; set; }

    // -(instancetype _Nonnull)initWithFrame:(CGRect)frame __attribute__((objc_designated_initializer));
    [Export("initWithFrame:")]
    [DesignatedInitializer]
    IntPtr Constructor(CGRect frame);

    // -(instancetype _Nullable)initWithCoder:(NSCoder * _Nonnull)aDecoder __attribute__((objc_designated_initializer));
    //[Export("initWithCoder:")]
    //[DesignatedInitializer]
    //IntPtr Constructor(NSCoder aDecoder);
}

// @interface SegmentedPagerTabStripViewController : PagerTabStripViewController
[BaseType(typeof(PagerTabStripViewController), Name = "_TtC15XLPagerTabStrip36SegmentedPagerTabStripViewController")]
interface SegmentedPagerTabStripViewController
{
    // @property (nonatomic, weak) UISegmentedControl * _Null_unspecified segmentedControl __attribute__((iboutlet));
    [Export("segmentedControl", ArgumentSemantic.Weak)]
    UISegmentedControl SegmentedControl { get; set; }

    // -(instancetype _Nonnull)initWithNibName:(NSString * _Nullable)nibNameOrNil bundle:(NSBundle * _Nullable)nibBundleOrNil __attribute__((objc_designated_initializer));
    [Export("initWithNibName:bundle:")]
    [DesignatedInitializer]
    IntPtr Constructor([NullAllowed] string nibNameOrNil, [NullAllowed] NSBundle nibBundleOrNil);

    // -(instancetype _Nullable)initWithCoder:(NSCoder * _Nonnull)aDecoder __attribute__((objc_designated_initializer));
    //[Export("initWithCoder:")]
    //[DesignatedInitializer]
    //IntPtr Constructor(NSCoder aDecoder);

    // -(void)viewDidLoad;
    [Export("viewDidLoad")]
    void ViewDidLoad();

    // -(void)reloadPagerTabStripView;
    [Export("reloadPagerTabStripView")]
    void ReloadPagerTabStripView();

    // -(void)updateIndicatorFor:(PagerTabStripViewController * _Nonnull)viewController fromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex;
    [Export("updateIndicatorFor:fromIndex:toIndex:")]
    void UpdateIndicatorFor(PagerTabStripViewController viewController, nint fromIndex, nint toIndex);

    // -(void)scrollViewDidEndScrollingAnimation:(UIScrollView * _Nonnull)scrollView;
    [Export("scrollViewDidEndScrollingAnimation:")]
    void ScrollViewDidEndScrollingAnimation(UIScrollView scrollView);
}

// @interface TwitterPagerTabStripViewController : PagerTabStripViewController
[BaseType(typeof(PagerTabStripViewController), Name = "_TtC15XLPagerTabStrip34TwitterPagerTabStripViewController")]
interface TwitterPagerTabStripViewController
{
    // -(instancetype _Nonnull)initWithNibName:(NSString * _Nullable)nibNameOrNil bundle:(NSBundle * _Nullable)nibBundleOrNil __attribute__((objc_designated_initializer));
    [Export("initWithNibName:bundle:")]
    [DesignatedInitializer]
    IntPtr Constructor([NullAllowed] string nibNameOrNil, [NullAllowed] NSBundle nibBundleOrNil);

    // -(instancetype _Nullable)initWithCoder:(NSCoder * _Nonnull)aDecoder __attribute__((objc_designated_initializer));
    //[Export("initWithCoder:")]
    //[DesignatedInitializer]
    //IntPtr Constructor(NSCoder aDecoder);

    // -(void)viewDidLoad;
    [Export("viewDidLoad")]
    void ViewDidLoad();

    // -(void)reloadPagerTabStripView;
    [Export("reloadPagerTabStripView")]
    void ReloadPagerTabStripView();

    // -(void)updateIndicatorFor:(PagerTabStripViewController * _Nonnull)viewController fromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex withProgressPercentage:(CGFloat)progressPercentage indexWasChanged:(BOOL)indexWasChanged;
    [Export("updateIndicatorFor:fromIndex:toIndex:withProgressPercentage:indexWasChanged:")]
    void UpdateIndicatorFor(PagerTabStripViewController viewController, nint fromIndex, nint toIndex, nfloat progressPercentage, bool indexWasChanged);

    // -(void)updateIndicatorFor:(PagerTabStripViewController * _Nonnull)viewController fromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex;
    [Export("updateIndicatorFor:fromIndex:toIndex:")]
    void UpdateIndicatorFor(PagerTabStripViewController viewController, nint fromIndex, nint toIndex);

    // -(void)observeValueForKeyPath:(NSString * _Nullable)keyPath ofObject:(id _Nullable)object change:(NSDictionary<NSKeyValueChangeKey,id> * _Nullable)change context:(void * _Nullable)context;
    [Export("observeValueForKeyPath:ofObject:change:context:")]
    unsafe void ObserveValueForKeyPath([NullAllowed] string keyPath, [NullAllowed] NSObject @object, [NullAllowed] NSDictionary<NSString, NSObject> change, [NullAllowed] NSObject context);

    // -(void)viewDidLayoutSubviews;
    [Export("viewDidLayoutSubviews")]
    void ViewDidLayoutSubviews();
}
