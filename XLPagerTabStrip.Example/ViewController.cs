﻿using System;

using UIKit;

namespace XLPagerTabStrip.Example
{
    public partial class ViewController : BarPagerTabStripViewController
    {
        protected ViewController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void ReloadPagerTabStripView()
        {
            base.ReloadPagerTabStripView();
        }
    }
}
